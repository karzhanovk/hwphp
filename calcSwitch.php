<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html" charset=utf-8">
	<title>SimpleCalculatorWithSwitch</title> 
</head>
<body>
	<h1>Калькулятор:</h1>
<?php
	if($_GET['submit']) 
	{
		$num_1 = (float)$_GET['first'];
		$num_2 = (float)$_GET['second'];
		$operation = $_GET['operation'];
		$result;

		switch ($operation) {
			case '+':
				$result = $num_1 + $num_2;
				break;
			case '-':
				$result = $num_1 - $num_2;
				break;
			case '*':
				$result = $num_1 * $num_2;
				break;
			case '/':
				$result = ($num_2 == '0') ? "you can't divide by zero" : $num_1 / $num_2;  			
			break;	
			case '^':
				$result = pow($num_1, $num_2);
			break;
			case '%':
				$result = fmod($num_1, $num_2);
			break;
			default:
				echo "invalid operation";
			break;
		}
	}
?> 
<form action="" method="get">
	<input type="number" name="first" placeholder="enter your value" required>
	<p></p>

		<select name="operation" required>
			<option value="+">+</option>
			<option value="-">-</option>
			<option value="/">/</option>
			<option value="*">*</option>
			<option value="^">^</option>
			<option value="%">%</option>
		</select>
	<p></p>
	<input type="number" name="second" placeholder="enter your value" required>
	<p></p>
	<input type="submit" name="submit" required>
</form>
<?php
	
	echo "= <p><b>$result</b></p>";
?>

</body>
</html>


