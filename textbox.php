<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SimpleTextbox</title> <!-- обязательно должен присутствовать в коде документа-->
</head>
<body>
	<h1>Заполните форму:</h1> <!--всегда начинаются с новой строки, а после них другие элементы отображаются на следующей строке. Кроме того, перед заголовком и после него добавляется пустое пространство.-->
<form action="" method="POST">
	<input type="text" name="firstname" placeholder="enter your firstname" required>
	<p></p> <!--Если закрывающего тега нет, считается, что конец абзаца совпадает с началом следующего блочного элемента.-->
	<input type="text" name="secondname" placeholder="enter your secondname" required>
	<p></p>
	<input type="text" name="address" placeholder="enter your address" required>
	<p></p>
	<input type="text" name="pet" placeholder="enter your pet name" required>
	<p></p>
	<input type="reset" name="reset"><input type="submit" name="submit" required>
</form>	

<?php
	if($_POST['submit'])
	{
		$fn = (string)$_POST['firstname'];
		$sn = (string)$_POST['secondname'];
		$adr= (string)$_POST['address'];
		$pet= (string)$_POST['pet'];

		echo "Hello <strong>$fn $sn</strong> your address is <em>$adr</em>. We'll treat your $pet";
		
	}
?> 
</body>
</html>