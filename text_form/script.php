<!DOCTYPE html>
<html>
<head>
	<title>FormUserData</title>

	<style>

		table {
				border-collapse: collapse;
		}

		th,
		td 	  {
				border: 1px solid #000;
				padding: 5px 10px;
		}

	</style>
</head>
<body>
	<?php include "action.php"?>
<table>

	<tr>
		<?php foreach ($userData as $th_key => $th_value): ?>
				<th><?php echo $th_key ?></th>	
		<?php endforeach; ?>

		<?php foreach ($arrUserList as $jsonUser): 
				$arrUser = json_decode($jsonUser, true);?>

	  		<tr>
				<?php foreach ($arrUser as $td_key => $td_value): ?>
			  		<td><?php echo $td_value ?></td>	
				<?php endforeach; ?>
			</tr>

		<?php endforeach; ?>

</table>

</body>
</html>