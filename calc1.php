<?php
	if($_GET['submit'])
	{
		$num_1 = (float)$_GET['first'];
		$num_2 = (float)$_GET['second'];
		$operation = $_GET['operation'];
		$result;

		if($operation=='+')
			$result = $num_1 + $num_2;
		else if($operation=='-')
			$result = $num_1 - $num_2;
		else if($operation=='*')
			$result = $num_1 * $num_2;
		else if($operation=='/')
		{
			if($num_2==0)
				echo "you can't divide by zero";
			else 
				$result = $num_1 / $num_2;
		}
		else if ($operation=='^')
			$result = pow($num_1, $num_2);
		else $result = fmod($num_1, $num_2);

	}
?> 
<form action="" method="get">
	<input type="number" name="first" placeholder="enter your value" required>
	<p></p>
		<select name="operation" required>
			<option value="+">+</option>
			<option value="-">-</option>
			<option value="/">/</option>
			<option value="*">*</option>
			<option value="^">^</option>
			<option value="%">%</option>
		</select>
	<p></p>
	<input type="number" name="second" placeholder="enter your value" required>
	<p></p>
	<input type="submit" name="submit" required>
</form>
<?php
	
	echo "= $result";
	//print_r($_POST);
?>
